This crate provides a tool to format a number in [Kaktovik numerals](https://en.wikipedia.org/wiki/Kaktovik_numerals), 
which are a base-20 system of numerical digits created by Alaskan Iñupiat.
To use it, you can create a [KaktovikNum](https://docs.rs/kaktovik/latest/kaktovik/struct.KaktovikNum.html) object 
with the number you want to format, and then print it out. Here's an example:

### Usage
```
use kaktovik::*;

fn main() {
    let k = KaktovikNum::new(859);
    println!("{}", k);
}
```

This will output "𝋂𝋂𝋓", which is the Kaktovik numeral representation of the number 859.
