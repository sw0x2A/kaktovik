use criterion::{black_box, criterion_group, criterion_main, Criterion};
use kaktovik::*;

pub fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("to_kaktovik", |b| {
        b.iter(|| KaktovikNum::new(black_box(859)).to_kaktovik())
    });
    c.bench_function("parse", |b| b.iter(|| KaktovikNum::parse(black_box("𝋂𝋂𝋓"))));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
